import React, { Component } from 'react'
import { connect } from 'react-redux'
import bindActionCreators from 'redux'

const Authorization = (allowedRoles) => (WrappedComponent) => {
    class withAuthorization extends Component{
        componentDidMount = () => {
            if(!this.props.userData){
                this.props.getUserData()
            }
        }

        render = () => {
            const { role } = this.props.userData
            if(allowedRoles.includes(role)) {
                return <WrappedComponent { ...this.props } />
            } else {
                return <h1>No page for u</h1>
            }
        }
    }

    mapStateToProps = state => {
        return {
            userData: state.userData
        }
    }

    mapDispatchToProps = dispatch => {
        return bindActionCreators({}, dispatch)
    }

    return connect(mapStateToProps)(withAuthorization)
}

export default Authorization