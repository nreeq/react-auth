import React, { Component } from 'react'
import Child from './child'

class App extends Component{
    render = () => {
        return (
            <div className='init'>
                <Child />
            </div>
        )
    }
}

export default App