import React, { Component } from 'react'
import Authorization from './../decorators/authentication'

class Child extends Component{
    render = () => {
        return (
            <div className='child'>
                child
            </div>
        )
    }
}

export default Authorization(['admin'])(Child);