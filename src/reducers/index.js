import { combineReducers } from 'redux'
import userReducer from './user-reducer'

const mainReducer = combineReducers({
    userData: userReducer
})

export default mainReducer