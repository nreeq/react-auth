const hardcodeUser = {
    username: 'retard',
    email: 'retard@gmail.com',
    role: ['admin']
}

const userReducer = (state = hardcodeUser, action) => {
    switch(action.type){
        case 'FETCH_USER_DATA':
            return state
        case 'FETCH_USER_SUCCESS':
            return state
        case 'FETCH_USER_ERROR':
            return state
        default:
            return state
    }
}

export default userReducer