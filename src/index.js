import React from 'react'
import ReactDOM, { render } from 'react-dom'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import './index.css'
import App from './application/app'
import mainReducer from './reducers'

const store = createStore(mainReducer)

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
)
